import Vue from 'vue'
import App from './App.vue'

import store from '@/store'

import router from '@/router'

// @ts-ignore
import vueKitPlugin from '@qr/vue-kit/src/plugins/vue-kit'
// @ts-ignore
import errorHandlingPlugin from '@qr/vue-kit/src/plugins/error-handling'

// @ts-ignore
import Paginate from "vuejs-paginate"

Vue.component('paginate', Paginate)

Vue.config.productionTip = false

Vue.use(vueKitPlugin)


const vm = new Vue({
    router,
    store,
    render: h => h(App)

})



Vue.use(errorHandlingPlugin, {
    errorHandlersParams: {
        vueInstance: vm,

        // apiErrorPreHandler(error: any) {
        //     // Ошибки авторизации игнорируются глобальным обработчиком
        //     return isAxiosAuthenticateError(error)
        // },

        getApiErrorModalOptions(error: any) {
            if (!error.response) return {}

            return {
                mainErrorText: undefined, // использовать стандартный текст
                additionalErrorTexts: [
                ],
                hideAcceptButton: false,
            }
        },
    },
})

vm.$mount('#app')