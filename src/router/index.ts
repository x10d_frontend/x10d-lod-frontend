import Vue from 'vue'
import VueRouter from 'vue-router'

import store from '@/store'

// @ts-ignore
import Students from '@/routerViews/Students.vue'

// @ts-ignore
import StudentDetail from '@/routerViews/StudentDetail.vue'

// @ts-ignore
import StudentUpdate from "@/routerViews/StudentUpdate"

// @ts-ignore
import StudentCreate from "@/routerViews/StudentCreate"

import Contest from '@/routerViews/Contest'

import ContestDetail from '@/routerViews/ContestDetail'

import ContestUpdate from '@/routerViews/ContestUpdate'

import ContestCreate from '@/routerViews/ContestCreate'

import Main from '@/routerViews/Main'

import Login from '@/routerViews/Login'

import Errors from '@/routerViews/Errors'

import {
    STUDENTS,
    CONTEST,
    CONTEST_UPDATE,
    CONTEST_CREATE,
    ERRORS,
    LOGIN,
    MAIN,
    STUDENT_UPDATE,
    STUDENT_CREATE
} from '@/router/route-paths'



Vue.use(VueRouter)

const routes = [
    { path: '/', redirect: { name: 'login' } },
    { path: LOGIN, name: 'login', component: Login },
    {
        path: MAIN,
        name: 'main',
        component: Main,
        children:[
            { path: '/', redirect: { name: 'students' } },
            { path: STUDENTS, name: 'students', component: Students },
            { path: CONTEST, name: 'contest', component: Contest },
            { path: CONTEST_UPDATE, name: 'contestUpdate', component: ContestUpdate },
            { path: CONTEST_CREATE, name: 'contestCreate', component: ContestCreate },
            { path: STUDENT_UPDATE, name: 'studentUpdate', component: StudentUpdate },
            { path: STUDENT_CREATE, name: 'studentCreate', component: StudentCreate },
        ]
    },
    { path: '/contest/:id', component: ContestDetail },
    { path: '/students/:id', component: StudentDetail },

    { path: ERRORS, name: 'Errors', component: Errors },
]

const router = new VueRouter({
    routes,
})

/**
 * Хук, в котором происходит получение данных о пользователе
 */
router.beforeEach(async (to, from, next) => {
    next()
})


/**
 * Хук, который перенаправляет на состояние вывода ошибок, в случае их наличия
 */
router.beforeEach(async (to, from, next) => {

    if (
        to.name !== 'Errors' &&
        store.getters.applicationHasErrors
    ) {
        next('/errors')
    }
    else {
        next()
    }

})

/**
 * Хук, который запрещает переход на состояние вывода ошибок, если их нет
 */
router.beforeEach(async (to, from, next) => {

    if (
        // Добавить проверку на наличие ошибок и редиректить в корень
        to.name === 'Errors' &&
        !store.getters.applicationHasErrors
    ) {
        next('/')
    }
    else {
        next()
    }
})

export default router
