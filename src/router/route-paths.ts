export const STUDENTS = '/students'

export const CONTEST = '/contest'

export const CONTEST_UPDATE = '/contestUpdate'

export const CONTEST_CREATE = '/contestCreate'

export const STUDENT_UPDATE = '/studentsUpdate'

export const STUDENT_CREATE = '/studentsCreate'

export const ERRORS = '/errors'

export const LOGIN = '/login'

export const MAIN = '/main'

