
export default interface ExceptionsState {

    currentStudent : {
        // id студента
        _id : string

        achivments : Array<string>

        tags:[]

        name : string

        surName : string

        lastName : string

        birthDate : string

        district : string

        city : string

        school : string

        schoolGroup : string
    }
}