import {
    Module,
} from 'vuex'

import axios from 'axios'

import RootState from '@/store/types/RootState'


const studentsModule : Module<any, RootState> = {

    state: {
        currentStudent: [],
    },

    mutations: {
        changeCurrentStudent(state: any, student) : void {
            state.currentStudent = student
        },
    },

    getters: {
        getCurrentStudent(state) {
            return state.currentStudent
        }
    },

    actions: {
        replaceCurrentStudent({ commit }, student) {
            commit('changeCurrentStudent', student)
        },

        updateStudent({}, student) {

            axios({
                url: 'api/student/update',
                method: 'post',
                data: student
            })

        },

        createStudent({}, student) {

            return axios({
                url: 'api/student/add',
                method: 'post',
                data: student
            })

        },
    }

}

export default studentsModule
