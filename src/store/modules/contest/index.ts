import {
    Module,
} from 'vuex'

import axios from 'axios'

import RootState from '@/store/types/RootState'


const contestModule : Module<any, RootState> = {

    state: {
        currentContest: null,
        dictionaries: {}
    },

    mutations: {
        changeCurrentContest(state: any, contest) : void {
            state.currentContest = contest
        },
        setDictionaries(state, dictionaries) : void {
            state.dictionaries = dictionaries
        }
    },

    getters: {

        getCurrentContest(state) {
            return state.currentContest
        },

        getDictionaries(state) {
            return state.dictionaries
        }
    },

    actions: {
        replaceCurrentContest({ commit }, contest) {
            commit('changeCurrentContest', contest)
        },

        updateContest({}, contest) {
            axios({
                url: 'api/contest/update',
                method: 'post',
                data: contest
            })

        },

        createContest({}, contest) {

            // @ts-ignore
            contest['directions'] = contest['directions'].map(item => item._id)
            // @ts-ignore
            contest['goals'] = contest['goals'].map(item => item._id)
            // @ts-ignore
            contest['educationLevels'] = contest['educationLevels']._id
            // @ts-ignore
            contest['profiles'] = contest['profiles'].map(item => item._id)

            axios({
                url: 'api/contest/add',
                method: 'post',
                data: contest
            })

        },

        removeContest({}, id) {

            axios({
                url: 'api/contest/delete',
                method: 'post',
                data: {
                    _id: id
                }
            })
        },

        sendPush({}, id) {
            axios({
                url: '/api/contest/notification/' + id,
            })
        },

        async getDictionaryList({commit}) {
            let dictionaries = await axios({
                url: '/api/dictionaries/list'
            }).then(item => item.data)

            commit('setDictionaries', dictionaries)
        }

    }

}

export default contestModule
