import {
    Module,
} from 'vuex'

import RootState from '@/store/types/RootState'


import {
    AUTHORIZE_ERROR_MESSAGE
} from './messages'


const errorsModule : Module<any, RootState> = {

    state: {
        applicationErrors: [],
    },

    mutations: {

        addAuthorizeError(state : any) : void {
            state.applicationErrors.push({
                message: AUTHORIZE_ERROR_MESSAGE,
            })
        },
    },

    getters: {

        applicationHasErrors(state : any) : boolean {
            return state.applicationErrors.length > 0
        },

        applicationErrors(state : any) : Array<any> {
            return state.applicationErrors
        },
    },

    actions: {
        addError(
            { commit }
        ) {
            commit('addAuthorizeError')
        }
    }

}

export default errorsModule
