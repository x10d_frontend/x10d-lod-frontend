import Vue from 'vue'
import Vuex from 'vuex'

import RootState from './types/RootState'

import errorsModule from './modules/errors'

import studentsModule from './modules/students'

import contestModule from './modules/contest'

Vue.use(Vuex)

export const strict = false

export default new Vuex.Store<RootState>({

    strict: process.env.NODE_ENV !== 'production',

    modules: {
        errors: errorsModule,
        students: studentsModule,
        contest: contestModule
    },

    state: {},

})
