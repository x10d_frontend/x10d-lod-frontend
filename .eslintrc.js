'use strict'

module.exports = {
    root: true,
    env: {
        browser: true,
        node: true,
    },
    extends: [
        '@qr',
        '@qr/eslint-config/typescript',
        '@qr/eslint-config/vue',
    ],
    settings: {
        'import/resolver': {
            webpack: {
                config: require.resolve('@vue/cli-service/webpack.config.js'),
            },
        },
    },
    rules: {

        // Специфичные для проекта правила

    },
}
