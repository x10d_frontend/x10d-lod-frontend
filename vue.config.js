'use strict'

const vueWebpackHelpers = require('@qr/vue-kit/src/utils/webpack')



module.exports = {
    lintOnSave: false,
    publicPath: '/',
    outputDir: undefined,
    assetsDir: undefined,
    runtimeCompiler: undefined,
    productionSourceMap: undefined,
    parallel: undefined,

    css: {
        sourceMap: true,
    },

    devServer: {
        // Тут адресс нашего локального мок сервера
        proxy: {
            "/" : {
                target: 'http://192.168.0.100:3000',
            }
        }
    },

    chainWebpack(config) {
        // С `*eval*` опциями в некоторых случаях
        // в Chrome не триггерится событие `unhandledrejection`
        // (см. https://bugs.chromium.org/p/v8/issues/detail?id=4874#c13)
        config.devtool('cheap-module-source-map')

        vueWebpackHelpers.chainVueSvgLoader(config)

    },
}
